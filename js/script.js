var fname,mname,lname,phoneno,emailid,city,state,stateoption,zipcode,getdata;


//method1.....................................
function formvalidation(){
  var printdata="";
  var name_regexp = /^[A-Za-z]{0,15}$/i;
  var phone_regexp = /^(\d{10})$/;
  //var phone_regexp =/^([0-9]{3})[-. ]([0-9]{3})[-. ]([0-9]{4})$/;
  var city_regexp = /^[A-Za-z]{3,15}$/i;
  var zipcode_regexp = /^(\d{6})$/;
  var email_regexp = /^\w+([.%-]?\w+)*@[A-Za-z]{3,}([.]{1}[A-Za-z]{2,6}){1,2}$/;
  //var email_regexp = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  //^[\w+-.%]+@[A-Za-z]{3,}([.]{1}[A-Za-z]{2,6}){1,2}$;
  fname = document.getElementById("firstname").value;
  mname = document.getElementById("middlename").value;
  lname = document.getElementById("lastname").value;
  phoneno = document.getElementById("phonenumber").value;
  city = document.getElementById("city").value;
  state = document.getElementById("state");
  stateoption = state.options[state.selectedIndex].value;
  zipcode = document.getElementById("zipcode").value;
  emailid = document.getElementById("emailid").value;
  
  
  // getdata = [fname,lname,phoneno,emailid,city,zipcode];  
  // for(var i=0;i<getdata.length;i++){
  //   printdata += "<li>" +getdata[i]+ "</li>";
  // }
  // document.getElementById("list").innerHTML =printdata;
  firstname(name_regexp,fname);
  middelname(name_regexp,mname);
  lastname(name_regexp,lname);
  emailaddress(email_regexp,emailid);
  phonenumber(phone_regexp,phoneno);
  cityname(city_regexp,city);
  statename(stateoption);
  zipcodeNumber(zipcode_regexp,zipcode);
  
  //document.getElementById("registerform").reset();
}
// firstname
function firstname(name_regexp,fname)
{
  if(name_regexp.test(fname)){   
    if(fname == "")
    {
      document.getElementById('firstname_error').innerHTML = "Please enter first name";
      returnvalue = false;
    }
    else{ 
      document.getElementById('firstname_error').innerHTML = "";
      returnvalue = true;
    }
  }
  else{       
      document.getElementById('firstname_error').innerHTML = "Invalid first name";
      returnvalue = false;
  }
   
  return returnvalue; 
}
// middelname
function middelname(name_regexp,mname)
{
  if(name_regexp.test(mname)){
    document.getElementById('middelname_error').innerHTML = "";
    returnvalue = true;
  }
  else{       
    document.getElementById('middelname_error').innerHTML = "Invalid Middel name";
    returnvalue = false;
  }
   
  return returnvalue; 
}
 // lasttname
function lastname(name_regexp,lname)
{
  if(name_regexp.test(lname)){   
    if(lname == "")
    {
      document.getElementById('lastname_error').innerHTML = "Please enter last name";
      returnvalue = false;
    }
    else{ 
      document.getElementById('lastname_error').innerHTML = "";
      returnvalue = true;
    }
  }
  else{       
      document.getElementById('lastname_error').innerHTML = "Invalid last name";
      returnvalue = false;
  }
   
  return returnvalue; 
}
// phonenumber
function phonenumber(phone_regexp,phoneno)
{
  if(phone_regexp.test(phoneno)){   
      document.getElementById('phonenumber_error').innerHTML = "";
      returnvalue = true;
  }
  else{     
    if(phoneno == "")
    {
      document.getElementById('phonenumber_error').innerHTML = "Please enter phone number";
      returnvalue = false;
    }
    else{  
      document.getElementById('phonenumber_error').innerHTML = "Invalid phone number";
      returnvalue = false;
    }
  }
   
  return returnvalue; 
}
// city
function cityname(city_regexp,city)
{
  if(city_regexp.test(city)){   
    document.getElementById('city_error').innerHTML = "";
    returnvalue = true;
  }
  else{       
    if(city == "")
    {
      document.getElementById('city_error').innerHTML = "Please enter city name";
      returnvalue = false;
    }
    else{ 
      document.getElementById('city_error').innerHTML = "Invalid city name";
      returnvalue = false;
    }
  }
   
  return returnvalue; 
}
//state
function statename(stateoption)
{
  if(stateoption==0)
  {
    document.getElementById('state_error').innerHTML = "Please select a state";
    returnvalue = false;
  }
  else
  {
    document.getElementById('state_error').innerHTML = "";
    returnvalue = true;
  }
   
  return returnvalue;
}
// phonenumber
function zipcodeNumber(zipcode_regexp,zipcode)
{
  if(zipcode_regexp.test(zipcode)){   
      document.getElementById('zipcode_error').innerHTML = "";
      returnvalue = true;
  }
  else{     
    if(zipcode == "")
    {
      document.getElementById('zipcode_error').innerHTML = "Please enter zipcode";
      returnvalue = false;
    }
    else{  
      document.getElementById('zipcode_error').innerHTML = "Please enter 6 digit zipcode";
      returnvalue = false;
    }
  }
   
  return returnvalue; 
}
// email
function emailaddress(email_regexp,emailid)
{
  if(email_regexp.test(emailid)){   
    document.getElementById('email_error').innerHTML = "";
    returnvalue = true;
  }
  else{   
    if(emailid == "")
    {
      document.getElementById('email_error').innerHTML = "Please enter Email";
      returnvalue = false;
    }
    else{    
      document.getElementById('email_error').innerHTML = "Invalid email";
      returnvalue = false;
    }
  }
   
  return returnvalue; 
}



// method2.....................................
// function setData(){
//   var printdata="";
//   var getdata =[];
//   var inputs = $(".form-control");
//   var getlabels =[];
//   var labels = $("#registerform label");
//   for(var i = 0; i < inputs.length; i++){
//     //getdata[i] =$(inputs[i]).val();
//     getdata.push($(inputs[i]).val());
//   }
//   for(var i = 0; i < labels.length; i++){
//     getlabels.push($(labels[i]).text());
//   }
//   for(var i=0;i<getdata.length;i++){
//     printdata += "<li>"  +getlabels[i]+ " = " +getdata[i]+ "</li>";
//   }
//   document.getElementById("list").innerHTML =printdata;
//   document.getElementById("registerform").reset();
// }

// function setData(){
//   var printdata="", getdata = $("#registerform").serializeArray();
//   for(var i=0;i<getdata.length;i++){
//     printdata += "<li>"  + getdata[i].name + " = " +getdata[i].value+ "</li>";
//   }
//   document.getElementById("list").innerHTML =printdata;
//   document.getElementById("registerform").reset();
// }
// function resetData(){
//   document.getElementById("registerform").reset();
//   document.getElementById("list").innerHTML ="";
//  // or $("#list").hide();
// }